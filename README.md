# OMOPCDM-Beacon

This project implements a Beacon v2 API on top of a OMOP CDM. It is derived from the [Beacon Reference Implementation API](https://github.com/costero-e/beacon2-ri-api).

## Before Running
You must have an OMOP CDM in a relational database based on PostgreSQL. You can change your default database URL in the [beacon2-ri-api-main/beacon/omop/__init__.py](beacon2-ri-api-main/beacon/omop/__init__.py) or change the URL in the environment variable `POSTGRES_URL`.

You can create a Python environment in the [beacon2-ri-api-main/](beacon2-ri-api-main/) with the [beacon2-ri-api-main/requirements.txt](beacon2-ri-api-main/requirements.txt) file.


### Installation

#### Docker

Before use docker you must export the environmental variable with the URL of your database. Remember to write `host.docker.internal` instead of `localhost`:

```
export POSTGRES_URL="postgresql://pgadmin:admin@host.docker.internal:5432/omopeunomiadb" # Url of your database. If not, it will use the default
```

For updating the metadata with your Beacon information, change the information in the [confTemplate.py](beacon2-ri-api-main/beacon/confTemplate.py) file and export the file as environment variable:

```
export BEACON_CONF=./beacon/confTemplate.py
```

You need [docker compose](https://docs.docker.com/compose/) to run the image of the script:

```
cd beacon2-ri-api-main/
docker compose up
```

#### Manual installation

Before use docker you must export the environmental variable with the URL of your database:

```
export POSTGRES_URL="postgresql://pgadmin:admin@localhost:5432/omopeunomiadb" # Url of your database. If not, it will use the default
```

For updating the metadata with your Beacon information, change the information in the [confTemplate.py](beacon2-ri-api-main/beacon/confTemplate.py) file and export the file as environment variable. If not, the default file will be fetched.

```
export BEACON_CONF=./beacon2-ri-api-main/beacon/confTemplate.py
```

Run the script (with Python >=3.5):

```
cd beacon2-ri-api-main/
python -m beacon
```

### Example GET Queries:

- Retrieve all individuals:

```
localhost:5050/api/individuals
```

- Retrieve one individual:

```
localhost:5050/api/individuals/{id}
```

- Filters:

```
http://localhost:5050/api/individuals?filters=SNOMED:444814009
```

```
http://localhost:5050/api/individuals?filters=Gender:F&filters=SNOMED:62106007
```


- Pagination and limit:

```
http://localhost:5050/api/individuals?skip=10&limit=5
```

### Example POST Queries:

- `request.json` file:

```json
{
    "meta": {
        "apiVersion": "2.0"
    },
    "query": {
        "filters": [
            {
                "id": "SNOMED:62106007",
                "includeDescendantTerms": true
            },
            {
                "id": "Gender:F",
                "includeDescendantTerms": true
            }
        ],
        "includeResultsetResponses": "HIT",
        "pagination": {
            "skip": 0,
            "limit": 10
        },
        "testMode": false,
        "requestedGranularity": "record"
    }
}

```

- Run the POST query:

```
http POST http://localhost:5050/api/individuals --json < request.json

```

You also can query by `biosamples` or `cohorts`. You have all possible queries in the [examples](examples/) section.

# Licencia

* GNU AFFERO GENERAL PUBLIC LICENSE Version 3. Ver [`LICENSE.md`](LICENSE.md).
* [![License: GPL-3.0](https://img.shields.io/badge/license-GPL--3.0-brightgreen)](https://www.gnu.org/licenses/gpl-3.0.en.html) 

# Agradecimientos

_El proyecto IMPaCT-Data (Exp. IMP/00019) ha sido financiado por el Instituto de Salud Carlos III, co-financiado por el Fondo Europeo de Desarrollo Regional (FEDER, “Una manera de hacer Europa”)._
